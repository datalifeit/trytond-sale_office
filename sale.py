# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.modules.company_office.office import OfficeMixin
from trytond.transaction import Transaction
from trytond.pyson import Eval, If, Bool


class Sale(OfficeMixin, metaclass=PoolMeta):
    __name__ = 'sale.sale'


class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'

    office = fields.Function(
        fields.Many2One('company.office', 'Office'),
        'get_office', searcher='search_office')

    @fields.depends('sale', '_parent_sale.office')
    def on_change_with_office(self):
        if self.sale and self.sale.office:
            return self.sale.office.id

    def get_office(self, name=None):
        if self.sale:
            return self.sale.office.id if self.sale.office else None

    @classmethod
    def search_office(cls, name, clause):
        return [
            ('sale.%s' % clause[0], ) + tuple(clause[1:])
        ]


class Sale2(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    def _get_invoice_sale(self):
        Journal = Pool().get('account.journal')

        invoice = super()._get_invoice_sale()

        if invoice:
            invoice.office = self.office
            if invoice.office and invoice.journal and \
                    invoice.office not in invoice.journal.offices:
                journals = Journal.search([
                    ('type', '=', invoice.journal.type)])
                for journal in journals:
                    if self.office in journal.offices:
                        invoice.journal = journal
                        break
        return invoice


class SaleLine2(metaclass=PoolMeta):
    __name__ = 'sale.line'

    def get_invoice_line(self):
        lines = super().get_invoice_line()
        for line in lines:
            line.office = self.sale.office
        return lines


class Sale3(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    @classmethod
    def set_number(cls, sales):
        for sale in sales:
            with Transaction().set_context(office_sequence=sale.office.id
                    if sale.office else None):
                super().set_number([sale])


class SaleLine3(metaclass=PoolMeta):
    __name__ = 'sale.line'

    def get_invoice_line(self):
        if self.sale.office:
            office = self.sale.office.id
        else:
            office = Transaction().context.get('office')
        with Transaction().set_context(office=office):
            return super().get_invoice_line()


class SaleLine4(metaclass=PoolMeta):
    __name__ = 'sale.line'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.product.domain.append(If(Bool(Eval('office')),
            ['OR',
                ('offices', '=', None),
                ('offices', '=', Eval('office'))
            ],
            []))
        cls.product.depends.append('office')


class Sale4(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    def _get_cost_invoice_line(self, cost, cost_lines,
            amount, default_values={}):
        invoice_line = super()._get_cost_invoice_line(
            cost, cost_lines, amount, default_values=default_values)
        if self.office:
            invoice_line.office = self.office.id
        else:
            invoice_line.office = Transaction().context.get('office')
        return invoice_line
