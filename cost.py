# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.transaction import Transaction
from trytond.model import fields

__all__ = ['SaleCost', 'SaleCost2', 'SaleCost3']


class SaleCost(metaclass=PoolMeta):
    __name__ = 'sale.cost'

    def _get_invoice_lines(self, apply_method):
        lines = super()._get_invoice_lines(apply_method)
        for line in lines:
            line.office = self.document.office
        return lines


class SaleCost2(metaclass=PoolMeta):
    __name__ = 'sale.cost'

    def _get_invoice_lines(self, apply_method):
        if self.document.office:
            office = self.document.office.id
        else:
            office = Transaction().context.get('office')
        with Transaction().set_context(office=office):
            return super()._get_invoice_lines(apply_method)


class SaleCost3(metaclass=PoolMeta):
    __name__ = 'sale.cost'

    office = fields.Function(
        fields.Many2One('company.office', 'Office'),
        'on_change_with_office', searcher='search_office')

    @fields.depends('document', '_parent_document.office')
    def on_change_with_office(self, name=None):
        if self.document:
            return self.document.office.id if self.document.office else None

    @classmethod
    def search_office(cls, name, clause):
        return [('document.%s' % clause[0], ) + tuple(clause[1:])]
