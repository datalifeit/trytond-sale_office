datalife_sale_office
====================

The sale_office module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-sale_office/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-sale_office)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
