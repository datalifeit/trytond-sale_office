====================================
Sale Office Account Product Scenario
====================================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import \
    ...     create_chart, get_accounts
    >>> from decimal import Decimal


Install sale_office::

    >>> config = activate_modules(['sale_office', 'account_office',
    ...     'office_account_product', 'sale_cost_apply_invoice'])


Create company::

    >>> Company = Model.get('company.company')
    >>> _ = create_company()
    >>> company = get_company()


Get user::

    >>> User = Model.get('res.user')
    >>> admin = User(config.user)


Create new user::

    >>> Group = Model.get('res.group')
    >>> admin_group, = Group.find([('name', '=', 'Administration')])
    >>> User = Model.get('res.user')
    >>> user = User()
    >>> user.name = 'User 1'
    >>> user.login = 'user1'
    >>> user.companies.append(company)
    >>> user.company = Company(company.id)
    >>> user.groups.append(admin_group)
    >>> user.save()


Create offices::

    >>> Office = Model.get('company.office')
    >>> office1 = Office()
    >>> office1.name = 'Office 1'
    >>> office1.company = company
    >>> office1.save()
    >>> office2 = Office()
    >>> office2.name = 'Office 2'
    >>> office2.company = company
    >>> office2.save()


Add office to admin user and reload context::

    >>> admin.offices.append(office1)
    >>> office1 = Office(office1.id)
    >>> admin.offices.append(office2)
    >>> office2 = Office(office2.id)
    >>> admin.office = office1
    >>> admin.save()
    >>> config._context = User.get_preferences(True, config.context)


Create accounts::

    >>> Account = Model.get('account.account')
    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> receivable = accounts['receivable']
    >>> revenue = accounts['revenue']
    >>> revenue2, = revenue.duplicate()
    >>> revenue2.name = 'Revenue 2'
    >>> revenue2.save()


Create customer::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer.account_receivable = receivable
    >>> customer.save()


Create account category::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name='Account Category')
    >>> account_category.accounting = True
    >>> account_category.account_revenue = revenue
    >>> account_category.save()
    >>> admin.office = office2
    >>> admin.save()
    >>> config._context = User.get_preferences(True, config.context)
    >>> account_category = ProductCategory(account_category.id)
    >>> account_category.account_revenue = revenue2
    >>> account_category.save()
    >>> admin.office = office1
    >>> admin.save()
    >>> config._context = User.get_preferences(True, config.context)


Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.default_uom = unit
    >>> template.type = 'service'
    >>> template.salable = True
    >>> template.list_price = Decimal('50')
    >>> template.cost_price = Decimal('20')
    >>> template.account_category = account_category
    >>> template.save()
    >>> product = template.products[0]


Create product service::

    >>> template = ProductTemplate()
    >>> template.name = 'service'
    >>> template.default_uom = unit
    >>> template.type = 'service'
    >>> template.salable = True
    >>> template.list_price = Decimal('50')
    >>> template.cost_price = Decimal('20')
    >>> template.cost_price_method = 'fixed'
    >>> template.account_category = account_category
    >>> template.save()
    >>> service = template.products[0]


Add office to journal::

    >>> Journal = Model.get('account.journal')
    >>> journal_revenue, = Journal.find([('code', '=', 'REV')])
    >>> journal_revenue.offices.append(office1)
    >>> office1 = Office(office1.id)
    >>> journal_revenue.offices.append(office2)
    >>> office2 = Office(office2.id)
    >>> journal_revenue.save()


Create cost types::

    >>> CostType = Model.get('sale.cost.type')
    >>> cost_type = CostType(name='Cost type')
    >>> cost_type.product = service
    >>> cost_type.apply_method = 'invoice_out'
    >>> cost_type.formula = '0.3*untaxed_amount'
    >>> cost_type.invoice_party = customer
    >>> cost_type.manual = True
    >>> cost_type.save()

    >>> cost_type2 = CostType(name='Cost Type Entry invoice')
    >>> cost_type2.product = service
    >>> cost_type2.formula = '0.1*quantity'
    >>> cost_type2.apply_method = 'sale_invoice'
    >>> cost_type2.manual = True
    >>> cost_type2.save()


Create sale::

    >>> Sale = Model.get('sale.sale')
    >>> sale = Sale()
    >>> sale.party = customer
    >>> sale.invoice_method = 'order'
    >>> sale_line = sale.lines.new()
    >>> sale_line.product = product
    >>> sale_line.quantity = 1.0
    >>> sale_line.unit_price = Decimal('5.0')
    >>> cost = sale.costs.new()
    >>> cost.type_ = cost_type
    >>> cost.office == sale.office
    True
    >>> cost2 = sale.costs.new()
    >>> cost2.type_ = cost_type2
    >>> for st in ['quote', 'confirm', 'process']:
    ...     sale.click(st)


Check invoice::

    >>> Invoice = Model.get('account.invoice')
    >>> InvoiceLine = Model.get('account.invoice.line')
    >>> invoice, = Invoice.find([])
    >>> iline1, iline2 = invoice.lines
    >>> iline1.account == revenue
    True
    >>> iline1.office == iline2.office == sale.office
    True


Check applied cost invoice line::

    >>> InvoiceLine = Model.get('account.invoice.line')
    >>> iline, = InvoiceLine.find(['origin', '=', 'sale.cost,%s' % sale.costs[0].id])
    >>> iline.office == sale.office
    True
    >>> iline.account == revenue
    True


Create sale with office2::

    >>> sale2 = Sale()
    >>> sale2.office = office2
    >>> sale2.party = customer
    >>> sale2.invoice_method = 'order'
    >>> sale_line = sale2.lines.new()
    >>> sale_line.product = product
    >>> sale_line.quantity = 1.0
    >>> sale_line.unit_price = Decimal('5.0')
    >>> cost = sale2.costs.new()
    >>> cost.type_ = cost_type
    >>> for st in ['quote', 'confirm', 'process']:
    ...     sale2.click(st)


Search costs by office::

    >>> SaleCost = Model.get('sale.cost')
    >>> len(SaleCost.find([('office', '=', office1)]))
    2
    >>> len(SaleCost.find([('office', '=', office2)]))
    1


Check invoice::

    >>> invoice2, = Invoice.find(['office', '=', office2])
    >>> iline2, = invoice2.lines
    >>> iline2.account == revenue2
    True


Check applied cost invoice line::

    >>> iline, = InvoiceLine.find(['origin', '=', 'sale.cost,%s' % sale2.costs[0].id])
    >>> iline.office == sale2.office
    True
    >>> iline.account == revenue2
    True


Check rules::

    >>> len(SaleCost.find([]))
    3
    >>> config.user = user.id
    >>> len(SaleCost.find([]))
    0
    >>> user.offices.append(office1)
    >>> office1 = Office(office1.id)
    >>> user.save()
    >>> config._context = User.get_preferences(True, config.context)
    >>> len(SaleCost.find([]))
    2
    >>> while user.offices:
    ...     _ = user.offices.pop()
    >>> user.offices.append(office2)
    >>> office2 = Office(office2.id)
    >>> user.save()
    >>> config._context = User.get_preferences(True, config.context)
    >>> len(SaleCost.find([]))
    1
    >>> user.offices.append(office1)
    >>> office1 = Office(office1.id)
    >>> user.save()
    >>> config._context = User.get_preferences(True, config.context)
    >>> len(SaleCost.find([]))
    3