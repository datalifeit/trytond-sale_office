====================
Sale Office Sequence
====================

Imports::

    >>> import datetime
    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard, Report
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> today = datetime.date.today()


Install sale_office::

    >>> config = activate_modules(['sale_office', 'office_sequence'])


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Create party::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer.save()


Create branch office::

    >>> Office = Model.get('company.office')
    >>> office1 = Office(name='Office 1')
    >>> office1.save()
    >>> office2 = Office(name='Office 2')
    >>> office2.save()
    >>> User = Model.get('res.user')
    >>> Group = Model.get('res.group')
    >>> user = User(config.user)
    >>> user.offices.extend([office1, office2])
    >>> office1 = Office(office1.id)
    >>> user.office = office1
    >>> user.save()


Add period to sale sequence::

    >>> SaleConf = Model.get('sale.configuration')
    >>> conf = SaleConf(1)
    >>> sequence = conf.sale_sequence
    >>> period = sequence.periods.new()
    >>> period.start_date = datetime.date(today.year, 1, 1)
    >>> period.end_date = datetime.date(today.year, 12, 31)
    >>> period.prefix = 'SOF1/'
    >>> period.office = office1
    >>> period2 = sequence.periods.new()
    >>> period2.start_date = datetime.date(today.year, 1, 1)
    >>> period2.end_date = datetime.date(today.year, 12, 31)
    >>> period2.prefix = 'SOF2/'
    >>> period2.office = office2
    >>> sequence.save()


Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')

    >>> template = ProductTemplate()
    >>> template.name = 'product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.salable = True
    >>> template.list_price = Decimal('10')
    >>> template.save()
    >>> product, = template.products


Create sales::

    >>> Sale = Model.get('sale.sale')
    >>> sale = Sale()
    >>> sale.sale_date = today
    >>> sale.office = office1
    >>> sale.party = customer
    >>> sale.invoice_method = 'order'
    >>> sale_line = sale.lines.new()
    >>> sale_line.product = product
    >>> sale_line.quantity = 2.0
    >>> sale.save()
    >>> sale.click('quote')
    >>> sale.number
    'SOF1/1'
    >>> sale2 = Sale()
    >>> sale2.sale_date = today
    >>> sale2.office = office2
    >>> sale2.party = customer
    >>> sale2.invoice_method = 'order'
    >>> sale_line = sale2.lines.new()
    >>> sale_line.product = product
    >>> sale_line.quantity = 2.0
    >>> sale2.save()
    >>> sale2.click('quote')
    >>> sale2.number
    'SOF2/1'